#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "./list.h"

void list_destroy(List *l) {
  while (!list_empty(l))
    (void) list_pop(l);
  free(l);
}

List *list_create(void) {
  List *l = (List *) malloc(sizeof(List));
  if (l == NULL) {
    perror("malloc");
    abort();
  } else {
    l->tail = NULL;
    l->head = NULL;
    l->size = 0;
  }
  return l;
}

State list_pop(List *l) {
  State state;
  Node *n = l->tail;
  if (l->size == 0) {
    fprintf(stderr, "ERROR: list_pop(), empty list\n");
    abort();
  }
  if (n->prev == NULL) {
    l->head = NULL;
    l->tail = NULL;
    l->size = 0;
  } else {
    l->tail = l->tail->prev;
    l->tail->next = NULL;
    l->size--;
  }
  state = n->state;
  free(n);
  return state;
}

void list_push(List *l, State state) {
  Node *n = (Node *) malloc(sizeof(Node));
  if (n == NULL) {
    perror("malloc");
    abort();
  }
  n->prev = NULL;
  n->next = NULL;
  n->state = state;
  if (l->size == 0) {
    l->head = n;
    l->tail = n;
    l->size = 1;
  } else {
    l->tail->next = n;
    n->prev = l->tail;
    l->tail = n;
    l->size++;
  }
}

State list_shift(List *l) {
  State state;
  Node *n = l->head;
  if (l->size == 0) {
    fprintf(stderr, "ERROR: list_shift(), empty list\n");
    abort();
  } else if (l->head->next == NULL) {
    l->head = NULL;
    l->tail = NULL;
    l->size = 0;
  } else {
    l->head = l->head->next;
    l->head->prev = NULL;
    l->size--;
  }
  state = n->state;
  free(n);
  return state;
}

void list_unshift(List *l, State state) {
  Node *n = (Node *) malloc(sizeof(Node));
  if (n == NULL) {
    perror("malloc");
    abort();
  }
  n->prev = NULL;
  n->next = NULL;
  n->state = state;
  if (l->size == 0) {
    l->head = n;
    l->tail = n;
    l->size = 1;
  } else {
    n->prev = NULL;
    n->next = l->head;
    l->head->prev = n;
    l->head = n;
    l->size++;
  }
}

unsigned list_size(List *l) {
  return l->size;
}

int list_empty(List *l) {
  return l->size == 0;
}

State list_at(List *l, unsigned position) {
  Node *n = l->head;
  if (position >= l->size) {
    fprintf(stderr, "ERROR: list_at(), out of bounds\n");
    abort();
  } else {
    while (position--) {
      n = n->next;
    }
    return n->state;
  }
}

int list_find(List *l, State state) {
  int position = 0;
  Node *n = l->head;
  while (n != NULL) {
    if (n->state.points_count == state.points_count) {
      if (memcmp(n->state.points, state.points, state.points_count) == 0) {
        return position;
      }
    } else {
      n = n->next;
      position++;
    }
  }
  return -1;
}

void list_insert(List *l, unsigned position, State state) {
  Node *n = l->head;
  if (position >= l->size) {
    fprintf(stderr, "ERROR: list_insert(), out of bounds\n");
    abort();
  } else {
    while (position--) {
      n = n->next;
    }
    n->state = state;
  }
}

void list_purge(List *l) {
  while (!list_empty(l))
    list_pop(l);
}
