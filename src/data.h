/* Copyright (c) 2010, Martin Kopta <martin@kopta.eu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* Basic data types used in project */

#ifndef _data_h_
#define _data_h_

/* messages passed between procesors */
typedef enum msgtype {
  MSG_PARSING_OK,
  MSG_PARSING_FAIL,
  MSG_REQUEST_WORK,
  MSG_WORK,
  MSG_NOWORK,
  MSG_BATON_WHITE,
  MSG_BATON_BLACK,
} Msgtype;

/* specifies type of passed message */
typedef enum tagtype {
  TAG_PARSING,
  TAG_DATA,
  TAG_SERVICE,
  TAG_BATON,
  TAG_END
} Tagtype;

/* color of processor
   used for termination
 */
typedef enum color {
  NONE = 0,
  BLACK,
  WHITE,
} Color;

typedef struct point {
  unsigned x;
  unsigned y;
} Point;

typedef struct state {
  unsigned points_count;
  Point *points;
  unsigned manhattan_distances;
} State;

struct {
  /* width of board */
  unsigned m;
  /* height of board */
  unsigned k;
  /* total number of points */
  unsigned b;
  /* number of fixed points */
  unsigned f;
  /* coordinates of fixed points */
  Point *f_cords;
  /* cached manhattan distances of fixed points */
  unsigned f_distances;
} Instance;

#endif /* _data_h_ */
