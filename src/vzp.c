/* Copyright (c) 2010, Martin Kopta <martin@kopta.eu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* Main file
   to understand VZP, start reading main
   keep in mind it's running on N computers simultaneously
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <mpi.h>

#include "./data.h"
#include "./list.h"

/* global vars */
unsigned long expanded_states_count = 0;

/* fwd declarations */
static State receive_state(const int dest);
static int parse_input_data(void);
static int solve(State *best, const int my_rank, const int proc_count, List
    *stack);
static unsigned manhattan_distances_fixed(void);
static unsigned manhattan_distances_fixed(void);
static void expand(State s, List *stack);
static void print_solution(State solution);
static void print_solution(State solution);
static void process_inbox(List *stack, Color *baton, Color *my_color, const int
    my_rank);
static void receive_instance(void);
static void save_if_best(State *best, const State s);
static void send_instance_to_all(const int proc_count);
static void send_state(State s, const int dest);



static void process_inbox(List *stack, Color *baton, Color *my_color, const int
    my_rank) {
  int msg, flag;
  MPI_Status status;

  MPI_Iprobe(MPI_ANY_SOURCE, TAG_BATON, MPI_COMM_WORLD, &flag, &status);
  if (flag) {
    MPI_Recv(&msg, 1, MPI_INT, status.MPI_SOURCE, TAG_BATON, MPI_COMM_WORLD,
        &status);
      *baton = msg == MSG_BATON_WHITE ? WHITE : BLACK;
  }

  MPI_Iprobe(MPI_ANY_SOURCE, TAG_SERVICE, MPI_COMM_WORLD, &flag, &status);
  if (flag) {
    MPI_Recv(&msg, 1, MPI_INT, status.MPI_SOURCE, TAG_SERVICE, MPI_COMM_WORLD,
        &status);
    if (msg == MSG_REQUEST_WORK) {
      if (list_size(stack) > 1) {
        if (status.MPI_SOURCE < my_rank)
          *my_color = BLACK;
        State s = list_shift(stack);
        msg = MSG_WORK;
        MPI_Send(&msg, 1, MPI_INT, status.MPI_SOURCE, TAG_SERVICE,
            MPI_COMM_WORLD);
        send_state(s, status.MPI_SOURCE);
        if (s.points)
          free(s.points);
      } else {
        msg = MSG_NOWORK;
        MPI_Send(&msg, 1, MPI_INT, status.MPI_SOURCE, TAG_SERVICE,
            MPI_COMM_WORLD);
      }
    }
  }
}

static void send_state(State s, const int dest) {
  MPI_Send(&s.points_count, 1, MPI_UNSIGNED, dest, TAG_DATA, MPI_COMM_WORLD);
  MPI_Send(&s.manhattan_distances, 1, MPI_UNSIGNED, dest, TAG_DATA,
      MPI_COMM_WORLD);
  MPI_Send(s.points, (int) s.points_count * 2, MPI_UNSIGNED, dest, TAG_DATA,
      MPI_COMM_WORLD);
}

static State receive_state(const int dest) {
  State s;
  MPI_Status status;
  MPI_Recv(&s.points_count, 1, MPI_UNSIGNED, dest, TAG_DATA, MPI_COMM_WORLD,
      &status);
  MPI_Recv(&s.manhattan_distances, 1, MPI_UNSIGNED, dest, TAG_DATA, MPI_COMM_WORLD,
      &status);
  s.points = (Point *) malloc(s.points_count * sizeof(Point));
  /* NOT CHECKING s.points for NULL by purpose */
  MPI_Recv(s.points, (int) s.points_count * 2, MPI_UNSIGNED, dest, TAG_DATA,
      MPI_COMM_WORLD, &status);
  return s;
}

static int parse_input_data(void) {
  unsigned i, j;
  if (scanf("%u", &(Instance.m)) != 1) {
    fprintf(stderr, "Bad input. Expected width of board (m).\n");
    return 1;
  }
  if (scanf("%u", &(Instance.k)) != 1) {
    fprintf(stderr, "Bad input. Expected height of board (k).\n");
    return 1;
  }
  if (scanf("%u", &(Instance.b)) != 1) {
    fprintf(stderr, "Bad input. Expected total number of points (b).\n");
    return 1;
  }
  if (Instance.b < 2) {
    fprintf(stderr, "Bad input. Expected at leas two points (b >= 2).\n");
    return 1;
  }
  if (scanf("%u", &(Instance.f)) != 1) {
    fprintf(stderr, "Bad input. Expected number of fixed points (f).\n");
    return 1;
  }
  Instance.f_cords = (Point *) malloc(sizeof(Point) * Instance.f);
  if (Instance.f_cords == NULL) {
    perror("malloc");
    return 1;
  }
  for (i = 0; i < Instance.f; i++) {
    if (scanf("%u", &(Instance.f_cords[i].x)) != 1) {
      fprintf(stderr,
          "Bad input. Expected X coordinate of %uth fixed point.\n", i);
      return 1;
    }
    if (Instance.f_cords[i].x > Instance.m - 1) {
      fprintf(stderr, "Bad input. Given X coordinate is out of board.\n");
      return 1;
    }
    if (scanf("%u", &(Instance.f_cords[i].y)) != 1) {
      fprintf(stderr,
          "Bad input. Expected Y coordinate of %uth fixed point.\n", i);
      return 1;
    }
    if (Instance.f_cords[i].y > Instance.k - 1) {
      fprintf(stderr, "Bad input. Given X coordinate is out of board.\n");
      return 1;
    }
    for (j = 0; j < i; j++) {
      if (Instance.f_cords[j].x == Instance.f_cords[i].x
          && Instance.f_cords[j].y == Instance.f_cords[i].y) {
        fprintf(stderr, "Bad input. Given point already exists.\n");
        return 1;
      }
    }
  }
  Instance.f_distances = manhattan_distances_fixed();
  return 0;
}

static void expand(State s, List *stack) {
  unsigned i, j, k, is_free;
  State new;
  for (i = 0; i < Instance.k; i++) {
    for (j = 0; j < Instance.m; j++) {

      is_free = 1;

      for (k = 0; k < Instance.f; k++) {
        if (Instance.f_cords[k].x == j && Instance.f_cords[k].y == i) {
          is_free = 0;
          break;
        }
      }
      for (k = 0; k < s.points_count; k++) {
        if (s.points[k].x == j && s.points[k].y == i) {
          is_free = 0;
          break;
        }
      }

      if (is_free) {
        unsigned distances = 0;
        int A, B;

        new.points_count = s.points_count + 1;

        new.points = (Point *) malloc(sizeof(Point) * (new.points_count));
        if (new.points == NULL) {
          perror("malloc");
          abort();
        }
        for (k = 0; k < s.points_count; k++)
          new.points[k] = s.points[k];
        new.points[k].x = j;
        new.points[k].y = i;

        for (k = 0; k < s.points_count; k++) {
          A = (int)j - (int)s.points[k].x;
          B = (int)i - (int)s.points[k].y;
          if (A < 0) A = -A;
          if (B < 0) B = -B;
          distances += (unsigned) (A + B);
        }
        for (k = 0; k < Instance.f; k++) {
          A = (int)j - (int)Instance.f_cords[k].x;
          B = (int)i - (int)Instance.f_cords[k].y;
          if (A < 0) A = -A;
          if (B < 0) B = -B;
          distances += (unsigned) (A + B);
        }

        new.manhattan_distances = s.manhattan_distances + 2 * distances;
        list_push(stack, new);
        expanded_states_count++;
      }

    }
  }
}

static unsigned manhattan_distances_fixed(void) {
  unsigned distances = 0, p, q;
  int A, B;
  for (p = 0; p < Instance.f; p++) {
    for (q = 0; q < Instance.f; q++) {
      A = ((int)Instance.f_cords[p].x - (int)Instance.f_cords[q].x);
      B = ((int)Instance.f_cords[p].y - (int)Instance.f_cords[q].y);
      if (A < 0) A = -A;
      if (B < 0) B = -B;
      distances += (unsigned) (A + B);
    }
  }
  return distances;
}

static void save_if_best(State *best, const State s) {
  if (s.manhattan_distances > best->manhattan_distances) {
    best->manhattan_distances = s.manhattan_distances;
    if (best->points) free(best->points);
    best->points_count = s.points_count;
    best->points = s.points;
  } else {
    /* if s isn't better than best, drop */
    if (s.points) free(s.points);
  }
}

static int solve(State *best, const int my_rank, const int proc_count, List *stack) {
  State s;
  int dest = (my_rank + 1) % proc_count, msg, baton_on_the_run = 0, flag;
  unsigned counter = 0;
  const int next_proc = (my_rank + 1) % proc_count;
  const unsigned inbox_check_threshold = 10;
  const unsigned max_points_count = Instance.b - Instance.f;
  Color my_color = WHITE, baton = NONE;
  MPI_Status status;

HAS_WORK:
  /* Active loop */
  while (!list_empty(stack)) {
    s = list_pop(stack);
    if (s.points_count < max_points_count) {
      expand(s, stack);
      if (s.points) free(s.points);
    } else {
      save_if_best(best, s);
    }
    if (counter++ == inbox_check_threshold) {
      process_inbox(stack, &baton, &my_color, my_rank);
      if (my_rank == 0 && baton != NONE)
        baton_on_the_run = 0;
      counter = 0;
    }
  }

  if (baton && my_rank) {
    if (my_color == BLACK) {
      msg = MSG_BATON_BLACK;
    } else {
      msg = baton == WHITE ? MSG_BATON_WHITE : MSG_BATON_BLACK;
    }
    MPI_Send(&msg, 1, MPI_INT, next_proc, TAG_BATON, MPI_COMM_WORLD);
    baton = NONE;
    my_color = WHITE;
  }

  if (my_rank == 0) {
    if (proc_count == 1)
      return 0;
    if (!baton_on_the_run) {
      msg = MSG_BATON_WHITE;
      MPI_Send(&msg, 1, MPI_INT, next_proc, TAG_BATON, MPI_COMM_WORLD);
      baton_on_the_run = 1;
    }
  }

  /* Passive loop */
  do {
    /* baton hadling */
    MPI_Iprobe(MPI_ANY_SOURCE, TAG_BATON, MPI_COMM_WORLD, &flag, &status);
    if (flag) {
      MPI_Recv(&msg, 1, MPI_INT, MPI_ANY_SOURCE, TAG_BATON, MPI_COMM_WORLD,
          &status);
      if (my_rank == 0) {
        baton_on_the_run = 0;
        if (msg == MSG_BATON_BLACK) {
          msg = MSG_BATON_WHITE;
          MPI_Send(&msg, 1, MPI_INT, next_proc, TAG_BATON, MPI_COMM_WORLD);
          baton_on_the_run = 1;
        } else if (msg == MSG_BATON_WHITE) {
          /* stop the world */
          int i;
          for (i = 1; i < proc_count; i++)
            MPI_Send(NULL, 0, MPI_INT, i, TAG_END, MPI_COMM_WORLD);
          return 0;
        }
      } else {
        /* forward given baton */
        if (my_color == BLACK) {
          msg = MSG_BATON_BLACK;
          my_color = WHITE;
        }
        MPI_Send(&msg, 1, MPI_INT, next_proc, TAG_BATON, MPI_COMM_WORLD);
      }
    }

    /* end handling */
    MPI_Iprobe(MPI_ANY_SOURCE, TAG_END, MPI_COMM_WORLD, &flag, &status);
    if (flag) {
      MPI_Recv(NULL, 0, MPI_INT, MPI_ANY_SOURCE, TAG_END, MPI_COMM_WORLD, &status);
      return 0;
    }

    /* work handling */
    int dest_reply = 0;
    msg = MSG_REQUEST_WORK;
    MPI_Send(&msg, 1, MPI_INT, dest, TAG_SERVICE, MPI_COMM_WORLD);
    do {
      if (my_rank > 0) {
        MPI_Iprobe(MPI_ANY_SOURCE, TAG_END, MPI_COMM_WORLD, &flag, &status);
        if (flag) {
          MPI_Recv(NULL, 0, MPI_INT, MPI_ANY_SOURCE, TAG_END, MPI_COMM_WORLD,
              &status);
          return 0;
        }
      }
      MPI_Iprobe(MPI_ANY_SOURCE, TAG_SERVICE, MPI_COMM_WORLD, &flag, &status);
      if (flag) {
        MPI_Recv(&msg, 1, MPI_INT, MPI_ANY_SOURCE, TAG_SERVICE, MPI_COMM_WORLD,
            &status);
        if (msg == MSG_REQUEST_WORK) {
          msg = MSG_NOWORK;
          MPI_Send(&msg, 1, MPI_INT, status.MPI_SOURCE, TAG_SERVICE,
              MPI_COMM_WORLD);
        } else if (status.MPI_SOURCE == dest) {
          if (msg == MSG_WORK) {
            list_push(stack, receive_state(dest));
            dest_reply = 1;
          } else if (msg == MSG_NOWORK) {
            dest_reply = -1;
          }
        }
      }
    } while (dest_reply == 0);

    if (dest_reply == 1)
      goto HAS_WORK;

    usleep(50000);
    dest = (dest + 1) % proc_count;
    if (dest == my_rank)
      dest = (dest + 1) % proc_count;
  } while (1);

  return 0;
}

static void print_solution(State solution) {
  unsigned i, j, k, found;
  (void) putchar('\n');
  for (i = 0; i < Instance.k; i++) {
    (void) putchar('\t');
    for (j = 0; j < Instance.m; j++) {
      found = 0;
      for (k = 0; k < Instance.f; k++) {
        if (Instance.f_cords[k].x == j && Instance.f_cords[k].y == i) {
          found = 1;
          break;
        }
      }
      for (k = 0; k < solution.points_count; k++) {
        if (solution.points[k].x == j && solution.points[k].y == i) {
          found = 2;
          break;
        }
      }
      if (found == 1) {
        (void) putchar('F');
      } else if (found == 2) {
        (void) putchar('O');
      } else {
        (void) putchar('.');
      }
      (void) putchar(' ');
      found = 0;
    }
    (void) putchar('\n');
  }
  (void) putchar('\n');
  printf("Manhattan distance: %u\n", solution.manhattan_distances);
  printf("Expanded states   : %lu\n", expanded_states_count);
  (void) fflush(stdout);
}


static void send_instance_to_all(const int proc_count) {
  int i;
  unsigned msg = MSG_PARSING_OK;
  /* stupid (= funny) */
  for (i = 1; i < proc_count; i++) {
    MPI_Send(&msg, 1, MPI_INT, i, TAG_PARSING, MPI_COMM_WORLD);
    MPI_Send(&Instance.m, 1, MPI_UNSIGNED, i, TAG_DATA, MPI_COMM_WORLD);
    MPI_Send(&Instance.k, 1, MPI_UNSIGNED, i, TAG_DATA, MPI_COMM_WORLD);
    MPI_Send(&Instance.b, 1, MPI_UNSIGNED, i, TAG_DATA, MPI_COMM_WORLD);
    MPI_Send(&Instance.f, 1, MPI_UNSIGNED, i, TAG_DATA, MPI_COMM_WORLD);
    MPI_Send(&Instance.f_distances, 1, MPI_UNSIGNED, i, TAG_DATA, MPI_COMM_WORLD);
    MPI_Send(Instance.f_cords, (int) Instance.f * 2, MPI_UNSIGNED, i,
        TAG_DATA, MPI_COMM_WORLD);
  }
}

static void receive_instance(void) {
  MPI_Status status;
  MPI_Recv(&Instance.m, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, TAG_DATA,
      MPI_COMM_WORLD, &status);
  MPI_Recv(&Instance.k, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, TAG_DATA,
      MPI_COMM_WORLD, &status);
  MPI_Recv(&Instance.b, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, TAG_DATA,
      MPI_COMM_WORLD, &status);
  MPI_Recv(&Instance.f, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, TAG_DATA,
      MPI_COMM_WORLD, &status);
  MPI_Recv(&Instance.f_distances, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, TAG_DATA,
      MPI_COMM_WORLD, &status);
  Instance.f_cords = (Point *) malloc(sizeof(Point) * Instance.f);
  /* NOT CHECKING Instance.f_cords for NULL by purpose */
  MPI_Recv(Instance.f_cords, (int) Instance.f * 2, MPI_UNSIGNED, MPI_ANY_SOURCE,
      TAG_DATA, MPI_COMM_WORLD, &status);
}

int main(int argc, char **argv) {
  int retval = 0,
      my_rank,  /* processor ID */
      p,  /* number of processors */
      i, msg;
  double t1, t2;
  State solution = {0, NULL, 0};

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &p);

  if (my_rank == 0) {
    printf("Number of processors = %d\n", p);

    if (argc != 1) {
      fprintf(stderr, "Usage: %s\n", *argv);
      msg = MSG_PARSING_FAIL;
      for (i = 1; i < p; i++)
        MPI_Send(&msg, 1, MPI_INT, i, TAG_PARSING, MPI_COMM_WORLD);
      MPI_Finalize();
      return 1;
    }

    retval = parse_input_data();

    if (retval != 0) {
      msg = MSG_PARSING_FAIL;
      for (i = 1; i < p; i++)
        MPI_Send(&msg, 1, MPI_INT, i, TAG_PARSING, MPI_COMM_WORLD);
      MPI_Finalize();
      return 1;
    }

    if (Instance.b <= Instance.f) {
      fprintf(stderr, "Nothing to solve (b <= f).\n");
      msg = MSG_PARSING_FAIL;
      for (i = 1; i < p; i++)
        MPI_Send(&msg, 1, MPI_INT, i, TAG_PARSING, MPI_COMM_WORLD);
      MPI_Finalize();
      return 1;
    } else if (Instance.b > Instance.m * Instance.k) {
      fprintf(stderr, "Nothing to solve (b > m*k)\n");
      msg = MSG_PARSING_FAIL;
      for (i = 1; i < p; i++)
        MPI_Send(&msg, 1, MPI_INT, i, TAG_PARSING, MPI_COMM_WORLD);
      MPI_Finalize();
      return 1;
    }

    send_instance_to_all(p);
    printf("Calculating.. ");
    (void) fflush(stdout);
  } else {
    MPI_Status status;
    MPI_Recv(&msg, 1, MPI_INT, MPI_ANY_SOURCE, TAG_PARSING, MPI_COMM_WORLD, &status);
    if (msg == MSG_PARSING_FAIL) {
      MPI_Finalize();
      return 1;
    } else {
      receive_instance();
    }
  }

  List *stack = list_create();
  if (my_rank == 0) {
    solution.manhattan_distances = Instance.f_distances;
    list_push(stack, solution);
  }
  t1 = MPI_Wtime();
  (void) solve(&solution, my_rank, p, stack);
  list_destroy(stack);

  MPI_Barrier(MPI_COMM_WORLD);

  if (my_rank == 0) {
    State s;
    for (i = 1; i < p; i++) {
      s = receive_state(i);
      if (s.manhattan_distances > solution.manhattan_distances) {
        if (solution.points)
          free(solution.points);
        solution = s;
      } else {
        if (s.points)
          free(s.points);
      }
    }
  } else {
    send_state(solution, 0);
  }

  MPI_Barrier(MPI_COMM_WORLD);

  if (my_rank == 0) {
    unsigned long ec;
    MPI_Status status;
    for (i = 1; i < p; i++) {
      MPI_Recv(&ec, 1, MPI_UNSIGNED_LONG, i, TAG_DATA, MPI_COMM_WORLD, &status);
      expanded_states_count += ec;
    }
  } else {
    MPI_Send(&expanded_states_count, 1, MPI_UNSIGNED_LONG, 0, TAG_DATA, MPI_COMM_WORLD);
  }
  t2 = MPI_Wtime();

  if (my_rank == 0) {
    print_solution(solution);
  }

  MPI_Barrier(MPI_COMM_WORLD);
  printf("%d: Elapsed time is %f.\n", my_rank, t2 - t1);

  if (solution.points)
    free(solution.points);
  if (Instance.f_cords)
    free(Instance.f_cords);

  MPI_Finalize();
  return retval;
}
