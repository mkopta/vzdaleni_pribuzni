/* Copyright (c) 2010, Martin Kopta <martin@kopta.eu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* (bidirectional) linked list header file
   in project it is used as stack
 */

#ifndef _list_h_
#define _list_h_

#include "./data.h"

/* list consists of many nodes */
typedef struct node {
  State state;
  struct node *prev;
  struct node *next;
} Node;

typedef struct {
  Node *head;
  Node *tail;
  unsigned size;
} List;

List *list_create(void);
void list_destroy(List *l);

/* pop from end of the list */
State list_pop(List *l);
/* push to the end of the list */
void list_push(List *l, State state);

/* take from begin of the list */
State list_shift(List *l);
/* insert at begin of the list */
void list_unshift(List *l, State state);

/* insert state at position (or abort) */
void list_insert(List *l, unsigned position, State state);

/* returns count of nodes in list */
unsigned list_size(List *l);

int list_empty(List *l);

/* finds given state in list
   returns position or -1 */
int list_find(List *l, State state);
/* returns state located at 'position' or aborts */
State list_at(List *l, unsigned position);

/* removes all nodes from list */
void list_purge(List *l);

#endif /* _list_h_ */
